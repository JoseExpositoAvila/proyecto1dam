---Primera---
Athletic Club	
Real Club Celta de Vigo	
Club Atl�tico de Madrid	
Real Club Deportivo Espa�ol	
Club Deportivo Legan�s	
Real Madrid Club de F�tbol	
Deportivo Alav�s	
Real Sociedad de F�tbol	
Catalu�a F�tbol Club Barcelona	
Real Valladolid Club de F�tbol	
Getafe Club de F�tbol	
Sevilla F�tbol Club	
Girona Futbol Club	
Sociedad Deportiva Eibar	
Levante Uni�n Deportiva	
Sociedad Deportiva Huesca	
Rayo Vallecano de Madrid	
Valencia Club de F�tbol	
Real Betis Balompi�	
Villarreal Club de F�tbol

---Segunda---

Albacete Balompi�
AD Alcorc�n
UD Almer�a
C�diz CF
C�rdoba CF
Deportivo de La Coru�a
Elche CF
Extremadura UD
Gimn�stic de Tarragona
Granada CF
UD Las Palmas
CD Lugo
M�laga CF
RCD Mallorca
CD Numancia
CA Osasuna
Rayo Majadahonda
Real Oviedo
Real Zaragoza
Club de Futbol Reus Deportiu
Sporting de Gij�n
CD Tenerife

---2�B Grupo 1---
R.C. Celta de Vigo "B"	
U.D. San Sebasti�n de los Reyes	
Coruxo F.C.	
A.D. Uni�n Adarve	
R.C. Deportivo Fabril	
Castilla y Le�n Burgos C.F.	
Pontevedra C.F.	
Cultural Leonesa	
R�pido de Bouzas	
C.D. Guijuelo	
Atl�tico de Madrid "B"	
S.D. Ponferradina	
C.F. Fuenlabrada	
C.F. Salmantino UDS	
Internacional de Madrid	
Unionistas de Salamanca C.F.	
Real Madrid Castilla	
Real Valladolid C.F. "B"	
C.D.A. Navalcarnero	
Las Palmas Atl�tico

---2�B Grupo 2---
U.P. Langreo	
S.D. Gernika	
Real Oviedo Vetusta	
S.D. Leioa	
Sporting de Gij�n "B"	
Real Sociedad "B"	
Gimn�stica de Torrelavega	
Real Uni�n Club	
Racing de Santander	
C.D. Vitoria	
S.D. Amorebieta	
C.D. Mirand�s	
Arenas Club	
C.D. Izarra	
Barakaldo C.F.	
C.D. Tudelano	
Bilbao Athletic	
C.D. Calahorra	
S.C.D. Durango	
U.D. Logro��s	

---2�B Grupo 3---
C.F. Badalona	
C.D. Castell�n	
F.C. Barcelona "B"	
H�rcules de Alicante C.F.	
U.E. Cornell�	
Ontinyent C.F.	
R.C.D. Espa�ol "B"	
Valencia C.F. Mestalla	
Club Lleida Esportiu	
Villarreal C.F. "B"	
U.E. Olot	
C.D. Atl�tico Baleares	
C.F. Peralada	
C.D. Ebro	
C.E. Sabadell F.C.	
S.D. Ejea	
C.D. Alcoyano	
C.D. Teruel	
Atl�tico Levante	
U.B. Conquense

---2�B Grupo 4---
U.D. Almer�a "B"	
U.D. Melilla	
Atl�tico Malague�o	
Islas Baleares U.D. Ibiza-Eivissa	
Atl�tico Sanluque�o C.F.	
F.C. Cartagena	
C.D. El Ejido 2012	
F.C. Jumilla	
R.B. Linense	
Real Murcia C.F.	
Marbella F.C.	
UCAM Murcia C.F.	
Recreativo Granada	
C.D. Badajoz	
R.C. Recreativo de Huelva	
C.D. Don Benito	
San Fernando C.D.	
C.F. Villanovense	
Sevilla Atl�tico	
C.F. Talavera de la Reina	

---3�Division Grupo 1 Galicia---
Alondras C.F. 
C.D. Arenteiro 
Arosa S.C. 
C.D. Barco 
Berganti�os C.F. 
C.D. Boiro 
C�ltiga F.C. 
C.D. Choco 
S.D. Compostela 
Laracha C.F. 
R.C. Ferrol 
Ourense C.F. 
U.D. Ourense 
U.D. Paiosaco 
Polvor�n F.C. 
Porri�o Industrial F.C. 
C.D. Ribadumia 
Silva S.D. 
U.D. Somozas 
R.C. Villalb�s

---3�Division Grupo2 Asturias---
Real Avil�s 
Caudal Deportivo 
U.C. Ceares 
C.D. Colunga 
Condal Club 
C.D. Covadonga 
U.D. Gij�n Industrial 
La Madalena de Morc�n C.F. 
C.D. Lealtad 
U.D. Llanera 
C.D. Llanes 
L'Entregu C.F. 
C. Marino de Luanco 
C.D. Mosconia 
C.D. Praviano 
U.D. San Claudio 
E.I. San Mart�n 
C. Siero 
C.D. Tuilla 
C.D. Universidad de Oviedo

---3�Division Grupo 3 Cantabria---

S.D. Atl�tico Albericia 
S.D. Barreda Balompi� 
C.D. Bezana 
C.D. Cay�n 
U.M. Escobedo 
C.D. Guarnizo 
C.D. Laredo 
C.D. Naval 
R. Racing Club "B" 
S.D. Revilla 
Ribamont�n al Mar C.F. 
F.C. Rinconeda Polanco 
U.D. S�mano 
A.D. Siete Villas 
S.D. Solares-Medio Cudeyo 
S.D. Textil Escudo 
S.D. Torina 
C.D. Tropez�n 
Velarde Camargo C.F. 
S.R.D. Vimenor C.F.

---3�Division Grupo 4 Pais Vasco---

D. Alav�s B 
Amurrio Club 
S.D. Balmaseda F.C. 
C.D. Basconia 
S.D. Beasain 
Bermeo F.T. 
S.D. Deusto 
C.D. Lagun Onak 
Ordizia K.E. 
Pasaia K.E. 
C. Portugalete 
Real Sociedad "C" 
C.F. San Ignacio 
S.D. San Pedro 
C.D. Santurtzi 
Santutxu F.C. 
Sestao R.C. 
Sodupe U.C. 
J.D. Somorrostro 
S.D. Zamudio

---3�Division Grupo 5 Catalu�a---
F.C. Asc� 
U.E. Castelldefels 
Cerdanyola del Vall�s F.C. 
C.E. Europa 
U.E. Figueres 
F.E. Grama 
E.C. Granollers 
U.A. Horta 
C.E. L'Hospitalet 
U.E. Llagostera-Costa Brava 
F.C. Martinenc 
C.F. Pobla de Mafumet 
A.E. Prat 
Reus Deportiu B 
C.P. San Crist�bal 
U.E. Sant Andreu 
FC Santboia 
Santfeliuenc F.C. 
U.E. Sants 
Terrassa Ol�mpica 
F.C. Vilafranca

---3�Division Grupo 6 Comunidad Valenciana---
C.D. Acero 
U.D. Alzira 
Atl�tico Saguntino 
Atzeneta U.E. 
Crevillente Deportivo 
Elche Ilicitano C.F. 
C.D. Eldense 
F.C. Jove Espa�ol San Vicente 
C.F. La Nuc�a 
Novelda C.F. 
C.D. Ol�mpic de X�tiva 
Orihuela C.F. 
Paiporta C.F. 
Paterna C.F. 
U.D. Rayo Ibense 
C.D. Roda 
Silla C.F. 
C.F. Torre Levante Orriols 
Vilamarxant C.F. 
Villarreal C.F. "C"

---3�Division Grupo 7 Comunidad de Madrid---
R.S.D. Alcal� 
F. Alcobendas Sport 
A.D. Alcorc�n "B" 
Atl�tico de Pinto 
C.D. Canillas 
R.C.D. Carabanchel 
Getafe C.F. "B" 
Las Rozas C.F. 
C.D. Legan�s "B" 
C.D. M�stoles U.R.J.C.
AD Parla 
C.F. Pozuelo de Alarc�n 
Rayo Vallecano "B" 
C.F. San Agust�n de Guadalix 
C.D. San Fernando de Henares 
D.A.V. Santa Ana 
C.D.F. Tres Cantos 
C.F. Trival Valderas 
C.D. Vic�lvaro 
S.R. Villaverde Boetticher C.F.

---3�Division Grupo 8 Castilla y Leon---
S.D. Almaz�n 
Arandina C.F. 
Atl�tico Astorga F.C. 
Atl�tico Bembibre 
Atl�tico Tordesillas 
Real �vila C.F. 
C.D. Beroil Bupolsa 
C.F. Briviesca 
C.D. Burgos Promesas 2000 
C.D. Cebrere�a 
Gimn�stica Segoviana C.F. 
C.D.L. J�piter Leon�s 
La Ba�eza F.C. 
C.D. La Granja 
C.D. La Virgen del Camino 
C.D. Numancia de Soria "B" 
C.D. Palencia Cristo Atl�tico 
U.D. Santa Marta de Tormes 
S.C. Uxama 
Zamora C.F.

---3�Division Grupo 9 Andalucia Oriental y Melilla---
Alhaur�n de la Torre C.F. 
C.D. Alhaurino 
Antequera C.F. 
Atarfe Industrial C.F. 
Atl�tico Mancha Real 
U.D. Ciudad de Torredonjimeno 
C.D. El Palo 
Guadix C.F. 
C.D. Hu�tor T�jar 
C.D. Hu�tor Vega 
Real Ja�n C.F. 
Juventud de Torremolinos C.F. 
Linares Deportivo 
Loja C.D. 
Martos C.D. 
CF Motril 
Poli Almer�a 
C.D. Rinc�n 
River Melilla C.F. 
U.D. San Pedro 
C.D. Torreperogil 
V�lez C.F.

---3�Division Grupo 10 Andalucia Occidental y Ceuta---
Algeciras C.F. 
Arcos C.F. 
Atl�tico Espele�o 
Betis Deportivo Balompi� 
C.D. Cabecense 
C�diz C.F. "B" 
A.D. Ceuta F.C. 
C.D. Ciudad de Lucena 
Conil C.F. 
C�rdoba C.F. "B" 
Coria C.F. 
�cija Balompi� 
C.D. Gerena 
C.D. Guadalcac�n 
U.B. Lebrijana 
U.D. Los Barrios 
Salerm Cosmetics Puente Genil 
C.D. San Roque de Lepe 
Sevilla F.C. "C" 
C.D. Utrera 
Xerez C.D. 
Xerez D.F.C.

---3�Division Grupo 11 Islas Baleares---
U.E. Alc�dia 
C.D. Binissalem 
C.E. Const�ncia 
C.E. Esporles 
C.E. Felanitx 
C.D. Ferriolense 
S.D. Formentera 
C.D. Ibiza Islas Pitiusas 
Ibiza Sant Rafel F.C. 
C.D. Llosetense
R.C.D. Mallorca "B"
C.D. Manacor 
C.E. Mercadal
C.D. Murense 
S.C.R. Pe�a Deportiva 
C.F. Platges de Calvi� 
U.D. Poblense 
Santa Catalina Atl�tico 
C.E. Santany� 
C.F. S�ller 
C.D. Son Cladera

---3�Division Grupo 12 Canarias---
Atl�tico Tacoronte 
Atl�tico Uni�n de G��mar 
C.D. Buzanada 
C.D. El Cotillo 
U.D. Ibarra 
C.D. La Cuadra 
U.D. Lanzarote 
U.D. Las Palmas "C" 
U.D. Las Zocas 
C.D. Marino 
C.D. Mensajero
C.F. Panader�a Pulido 
U.D. San Fernando 
C.D. Santa �rsula 
U.D. Tamaraceite 
C.D. Tenerife "B" 
S.D. Tenisca 
C.D. Uni�n Sur Yaiza 
C.F. Uni�n Viera 
U.D. Villa de Santa Br�gida

---3�Division Grupo 13 Region de Murcia---
�guilas F.C. 
C.D. Algar 
Atl�tico Pulpile�o 
F.C. Cartagena "B" 
E.D.M.F. Churra 
C.D. Cieza 
C.A.P. Ciudad de Murcia 
Deportiva Minera 
N.V. Estudiantes de Murcia C.F. 
Hu�rcal-Overa C.F. 
La Uni�n Atl�tico 
C.F. Lorca Deportiva 
Lorca F.C. 
U.D. Los Garres 
Mar Menor F.C. 
Mazarr�n F.C. 
S.F.C. Minerva 
Mule�o C.F. 
Ol�mpico de Totana
Real Murcia Imperial
UCAM Murcia C.F. "B"
Yeclano Deportivo

---3�Division Grupo 14 Extremadura---
E.M.D. Aceuchal 
Arroyo C.P. 
Atl�tico Pueblonuevo 
C.D. Azuaga 
C.P. Cacere�o 
C.D. Calamonte 
C.D. Castuera 
C.D. Coria 
C.D. Diocesano 
Extremadura U.D. "B" 
Jerez C.F. 
A.D. Llerenense 
A.D. M�rida 
U.D. Montijo 
Moralo C.P. 
Olivenza F.C. 
U.P. Plasencia 
Racing C. Pvo. Valverde�o 
C.D. Valdelacalzada 
C.P. Valdivia

---3�Division Grupo 15 Navarra---
C.D. Alesves 
C.D. Ardoi 
Atl�tico Cirbonero 
C.D. Avance Ezcabarte 
C.D. Bazt�n
C.D. Beti Onak 
Beti Kozkor K.E. 
U.C.D. Burlad�s
C.D. Cantolagua
U.D.C. Chantrea 
C.D. Corellano 
C.D. Cortes 
C.D. Huarte 
U.D. Mutilvera 
Atl�tico Osasuna "B" 
C.D. Pamplona 
Pe�a Sport F.C.
A.D. San Juan
C.D. Subiza
C.D. Valle de Eg��s

---3�Division Grupo 16 La Rioja---
C.D. Agoncillo
C.D. Alberite
C.D. Alfaro
C.D. Anguiano
C.D. Arnedo
Atl�tico River Ebro
Atl�tico Vian�s
C.D. Autol
C.D. Berceo
C.P. Calasancio
Haro Deportivo
C.D.F.C. La Calzada
S.D. Logro��s
U.D. Logro��s Promesas
N�xara C.D.
S.D. Oyonesa
C.D. Pradej�n
C.F R�pid de Murillo
C.D. Varea
Yag�e C.F.

---3�Division Grupo 17 Aragon---
A.D. Almud�var
Atl�tico de Monz�n
C.D. Belchite 97
C.D. Bin�far
S.D. Borja
C.D. Brea
C.F. Calamocha
U.D. Casetas
R.Z. Deportivo Arag�n
C.F. Illueca
C.D. La Almunia
C.D. Robres
A.D. Sabi��nigo
A.D. San Juan
UD San Lorenzo 
C.D. Sari�ena
C.D.J. Tamarite
S.D. Tarazona
Utebo F.C.
Villanueva C.F.

---3�Division Grupo 18 Castilla La Mancha---
Atl�tico Albacete
Almagro C.F.
Atl�tico Iba��s
CD Atl�tico Tomelloso
C.D. Azuqueca
C.D.B. Calvo Sotelo
C.D. Guadalajara
La Roda C.F.
C.F. La Solana
C.D. Madridejos
C.D. Manchego
C.D. Marchamalo
Mora C.F.
C.D. Quintanar del Rey
U.D. Socu�llamos C.F.
C.D. Taranc�n
C.D. Toledo
C.D. Villaca�as
C.P. Villarrobledo
Villarrubia C.F.