package simulador;

import clases.Division;
import clases.Equipo;
import clases.Jornada;
import clases.Partido;
import interfaces.Ventana;

public class Simulador {

	public static void main(String[] args) {
		
		
		
		Equipo []equipos = new Equipo[20];
		equipos[0] = new Equipo("Athletic Club");
		equipos[1] = new Equipo("Celta de Vigo");
		equipos[2] = new Equipo("Atletico de Madrid");
		equipos[3] = new Equipo("Deportivo Espa�ol");
		equipos[4] = new Equipo("Leganes");
		equipos[5] = new Equipo("Real Madrid");
		equipos[6] = new Equipo("Deportivo Alaves");
		equipos[7] = new Equipo("Real Sociedad");
		equipos[8] = new Equipo("Futbol Club Barcelona");
		equipos[9] = new Equipo("Real Valladolid");
		equipos[10] = new Equipo("Getafe");
		equipos[11] = new Equipo("Sevilla");
		equipos[12] = new Equipo("Girona");
		equipos[13] = new Equipo("Eibar");
		equipos[14] = new Equipo("Levante");
		equipos[15] = new Equipo("Huesca");
		equipos[16] = new Equipo("Rayo Vallecano");
		equipos[17] = new Equipo("Valencia");
		equipos[18] = new Equipo("Real Betis Balompie");
		equipos[19] = new Equipo("Villarreal");
		
		
		
		Division primera = new Division("Primera",1, equipos);
		primera.mostrarDatos();
		
	
		
		
		
		Jornada jornadasPrimera=new Jornada(equipos);
		
		 for (int i = 0; i < 19; i++) {
	            jornadasPrimera.mostrarJornada();
	            jornadasPrimera.setJornada();
	            
	        }  
	
	}

}
