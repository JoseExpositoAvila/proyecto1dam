package interfaces;

import javax.swing.JFrame;

public class Ventana extends JFrame {
	
	private Inicio inicio;
	
	
	public Ventana() {
		super();
		this.setTitle("inicio");
		inicio = new Inicio (this);
		this.setSize(450,350);
		this.setVisible(true);		
		this.setContentPane(inicio);
	}

}
