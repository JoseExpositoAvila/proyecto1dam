package interfaces;

import java.awt.Color;

import javax.swing.JButton;
import javax.swing.JPanel;

public class Inicio extends JPanel {

	private Ventana ventana;

	
	public Inicio(Ventana v) {
		super();
		ventana=v;
		this.setSize(450,338);
		setLayout(null);
		
		JButton boton1 = new JButton("Inicio");
		boton1.setForeground(Color.RED);
		boton1.setBackground(new Color(255, 215, 0));
		
		boton1.setBounds(163, 75, 89, 23);
		
		add(boton1);
		
		JButton boton2 = new JButton("Cargar");
		boton2.setBackground(Color.ORANGE);
		boton2.setBounds(163, 169, 89, 23);
		add(boton2);
	}
}
