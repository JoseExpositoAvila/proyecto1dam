package clases;

public class Equipo {

	private String nombre;
	private int calidadDelanteros;
	private int calidadCentrocampistas;
	private int calidadDefensas;
	private int calidadPorteros;
	private int calidadMedia;
	
	public Equipo(String nombre_equipo) {
		this.nombre=nombre_equipo;
		setCalidadDelanteros();
		setCalidadCentrocampistas();
		setCalidadDefensas();
		setCalidadPorteros();
		setCalidadMedia();
	}
	
	public void setCalidadDelanteros() {
		calidadDelanteros = (int)(Math.random()*(100-80+1)+80);
	}
	
	public void setCalidadCentrocampistas() {
		calidadCentrocampistas = (int)(Math.random()*(100-80+1)+80);
	}
	
	public void setCalidadDefensas() {
		calidadDefensas = (int)(Math.random()*(100-80+1)+80);
	}
	
	public void setCalidadPorteros() {
		calidadPorteros = (int)(Math.random()*(100-80+1)+80);
	}
	
	public void setCalidadMedia() {
		calidadMedia = (calidadDelanteros+calidadCentrocampistas+calidadDefensas+calidadPorteros)/4;
	}
	
	public int getCalidad() {
		
		return calidadMedia;
	}
	
	public String getNombre() {
		
		
		return nombre;
	}
	
	public void mostrar() {
		
		System.out.println("El equipoo se llama : "+nombre);
		System.out.println("La calidad de los delanteros es: "+calidadDelanteros);
		System.out.println("La calidad de los centrocampistas es: "+calidadCentrocampistas);
		System.out.println("La calidad de los defensas es: "+calidadDefensas);
		System.out.println("La calidad de los porteros es: "+calidadPorteros);
		System.out.println("La calidad media del "+nombre+" es: "+calidadMedia);
	}
}
