package clases;

public class Division {

	private String nombre;
	private int categoria;
	private Equipo[] equipo;
	
	
	
	public Division(String nombreDivision, int categoria, Equipo[] club ) {
		this.nombre=nombreDivision;
		this.categoria=categoria;
		this.equipo=club;
		mostrarDatos();
		
	}
	
	
	
	public void mostrarDatos() {
		
		System.out.println("La liga es: "+nombre+" . Es la "+categoria+" categoria en Espa�a"+" y los equipos son: ");
			for (int i = 0; i < equipo.length; i++) {
				equipo[i].mostrar();
				System.out.println(" ");
			}
	}
}
